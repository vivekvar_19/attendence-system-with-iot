**WHAT IS GIT**

    > It is a source code versioning system that lets you locally track changes and push or pull changes from remote resources.
    
**WHAT IS GITLAB**

    > Gitlab is a service that provides remote access to Git repositories. In addition to hosting your code, the services provide additional features designed to 
    help manage the software development lifecycle. These additional features include managing the sharing of code between different people, bug tracking, wiki 
    space and other tools for 'social coding'.
    
GitLab is great way to manage git repositories on centralized server. GitLab gives you complete control over your repositories or projects and allows you to decide 
whether they are public or private for free.

*FEATURES OF GITLAB*

    - GitLab offers free public and private repositories, issue-tracking and wikis.
    
    - GitLab hosts your (private) software projects for free.
    
    - GitLab is a platform for managing Git repositories.

*ADVANTAGES OF GITLAB*

    - GitLab provides unlimited number of private and public repositories for free.
    
    - GitLab provides GitLab Community Edition version for users to locate, on which servers their code is present.
    
*DISADVANTAGES OF GITLAB*

    - While pushing and pulling repositories, it is not as fast as GitHub.
    
    - GitLab interface will take time while switching from one to another page.
    
*GIT COMMANDS*

    1. To check git version :- $ git --version
    
    2. Add Git username and email address to identify the author while committing the information :- $ git config --global user.name "USERNAME"

    3. Verify the entered username by :- $ git config --global user.name
    
    4. Setting the e-mail address :- $ git config --global user.email "email_address@example.com"
    
    5. Verify the entered e-mail address :- $ git config --global user.email
    
    6. Use below command to check entered info :- $ git config --global --list

    7. To pull the latest changes made to the master branch :- $ git checkout master
    
    8. To fetch the latest changes to the working directory :- $ git pull origin NAME-OF-BRANCH -u
    
    9. Create a new branch :- $ git checkout -b branch-name

    10. To switch from one branch to other branch :- $ git checkout branch-name

    11. Check the changes made to your files :- $ git status

    12. Add all the files to staging :- $ git add *

    13. Send your changes to master branch :- $ git push origin branch-name
    
    14. To delete the all changes :- $ git clean -f

    15. To merge the different branch with the master branch :- $git checkout master
                                                                $ git merge branch-name 
                                                                
                                                                
*FORKING A PROJECT*

    1. To fork a project, click on the Fork button above the repository.
    
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/fork-project1.jpg)
    
    2. After forking the project, you need to add the forked project to a fork group.
    
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/fork-project2.jpg)
    
    3. Next it will start processing of forking a project.
    
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/fork-project3.jpg)
    
    4. It will display the success message after completion of forking.
    
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/fork-project4.jpg)
    
    
*CREATING A FILE* 

    1. To create a file by using command line interface, type the below command in your project directory.
    
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/create-file1.jpg)
                                                                          
    2. Now, you will see the file in your project directory.
    
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/create-file2.jpg)
    
*ADDING A USER TO A PROJECT*

    1. Click on the Members option under Settings tab.
    
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/adding-users1.jpg)
    
    2. Now enter the user name, role permission, expiration date(optional) and click on Add to project button to add the user to project.
    
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/adding-users3.jpg)
    
    3. You will get a successful message after adding user to the project.
    
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/adding-users4.jpg)
    
*REMOVING A USER*
    
    1. Click on the Members option under Settings tab.
    
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/remove-user1.jpg)
    
    2. You will see the list of users under Existing members and groups section and click on the delete option at right side to remove the user from project.
    
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/remove-user2.jpg)
    
    3.After clicking remove button, it will display a pop-up window saying whether to remove the selected user from the project or not. Click on Ok button 
      to remove the user.
      
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/remove-user3.jpg)
      
    4. Now, it will display the success message after removing the user from the project.
    
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/remove-user4.jpg)
    
*CREATING ISSUES*

    1. Go to Issues tab and click on the New issue button to create a new issue.
    
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/create-issue1.jpg)
    
    2. Now, fill the information such as title, description and if you want, you can select a user to assign an issue, milestone, labels upon operation or 
       could be choose by developers themselves later.
       
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/create-issue2.jpg)
    
    3. Click on the Submit issue button and you will get an overview of an issue along with title and description.
    
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/create-issue3.jpg)
    
*MERGING A REQUEST*

    1. Click on the Merge Requests tab and then click on the New merge request button.
    
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/merge-request1.jpg)
    
    2. To merge the request, select the source branch and target branch from the dropdown and then click on the Compare branches and continue button.
    
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/merge-request2.jpg)
    
    3. You will see the title, description and other fields such as assigning user, setting milestone, labels, source branch name and target branch name and 
       click on the Submit merge request button.
       
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/merge-request3.jpg)
       
    4. After submitting the merge request, you will get a new merge request screen.
    
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/merge-request4.jpg)
    
*CREATING WIKI PAGE*

    1.  Enter the title, format, fill the content section, add a commit message and then click on the Create page button.
    
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/wikipage1.jpg)
    
    2. You will get newly created wiki page.
    
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/wikipage2.jpg)


    
