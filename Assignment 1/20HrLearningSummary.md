                                                            **20 HOURS TO LEARN SOMETHING VIDEO SUMMARY** 
                                                        
Video Link :- [https://youtu.be/5MgBikgcWnY](url)

Video By :- JOSH KAUFMAN

![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images/IMG_20200627_144220.jpg)


**INTRODUCTION**
        
        In this video, the speaker JOSH KAUFMAN awares us about the learning process of our community i.e., humans. He proves that the time a person take in order to 
        learn a newskill is no more than 20 hours invested with hard work and dedication.
        Accorrding to him, 
            > It takes around twenty hours of practice to go from knowing absolutely nothing about what you’re trying to do to performing noticeably well. It doesn’t 
            matter whether you want to learn a language write a novel, paint a portrait, start a business, or fly an airplane. If you invest as little as twenty hours 
            in learning the basics of the skill, you’ll be surprised at how good you can become.
            

Many other books state that a normal human requires 10,000 hours to learn something. Various other websites also say this fact.

![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images/IMG_20200627_144251.jpg)

Graph on performance v/s practice time :-

![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images/IMG_20200627_144312.jpg)

According to speaker, this fact is indeed true but 10,000 hours of learning are required to become master or expert at anything but to be good at anything, it only 
requires 20 hours of dedication.

![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images/IMG_20200627_144354.jpg)

Graph on growth in learning v/s practice time :-

![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images/IMG_20200627_144330.jpg)

**Steps to rapid skill acquisition**

        1. Deconstruct the skill
        2. Learn enough to self - correct
        3. Remove practice barriers
        4. Practice at least 20 hours
        
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images/IMG_20200627_144413.jpg)

![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images/IMG_20200627_144434.jpg)

![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images/IMG_20200627_144450.jpg)

![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images/IMG_20200627_144505.jpg)

![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images/IMG_20200627_144522.jpg)
        
    **DECONSTRUCT THE SKILL**
        > To learn a skill, you must deconstruct the skill into its constituent subskills and learn enough about each subskill to be able to practice effectively and 
        self-correct. For instance, Kaufman finds a shortcut to learning how to play the ukulele by memorizing the three chords needed for the majority of songs, 
        which happen to be C, F, and G.
        
        Dividing a big task into smaller chunks makes the task easy.
        
    **LEARN ENOUGH TO SELF - CORRECT**
        > Sitting with all the resources on table to learn the whole skill by reading or viewing websites is not possible. The person must learn those things only which 
        enough to self - correct mistakes.
        
    **REMOVE PRACTICE BARRIERS**
        > While learning, various barriers often cross our path and we need to remove those barriers so that they don't alter our understanding.
        These barriers can be TV, Internet, Phone etc., depending on skill to skill.
        
    **PRACTICE AT LEAST 20 HOURS**
        > As the proverb says, *Practice makes the man perfect*.
        Therefore, practice is required to be at least good in a skill. Practicing doesn't mean just cramming but it means understanding the concept and doing hardwork 
        with dedication.
        
And at last, always remember that :-

![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images/IMG_20200627_144535.jpg)