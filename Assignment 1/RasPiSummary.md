**WHAT IS A RASPBERRY PI**

    > The Raspberry Pi is a low cost, credit-card sized computer that plugs into a computer monitor or TV, and uses a standard keyboard and mouse. It is a 
    capable little device that enables people of all ages to explore computing, and to learn how to program in languages like Scratch and Python. It’s 
    capable of doing everything you’d expect a desktop computer to do, from browsing the internet and playing high-definition video, to making spreadsheets,
    word-processing, and playing games. The Raspberry Pi  has the ability to interact with the outside world, and has been used in a wide array of digital 
    maker projects, from music machines and parent detectors to weather stations and tweeting birdhouses with infra-red cameras.

The latest version released is a 40 pin model B (8 GiB), released in 2020.

Image of Raspberry Pi :-

![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/r1.png)

**COMPONENTS OF RASPBERRY PI**

Like any computer, the Pi is made up of various different components, each of which has a role to play in making it work.

1. *Sytem on Chip (SoC)*
    > The name system-on-chip is a great indicator of what you would find if you prised the metal cover off: a silicon chip, known as an integrated circuit, 
which contains the bulk of the Raspberry Pi’s system. This includes the central processing unit (CPU), commonly thought of as the ‘brain’ of a computer, 
and the graphics processing unit (GPU), which handles the visual side of things.

![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/image--010.png)

2. *RAM*
    >  When you’re working on the Pi, it’s the RAM that holds what you’re doing; only when you save your work will it be written to the microSD card. Together,
these components form the Pi’s volatile and non-volatile memories: the volatile RAM loses its contents whenever the Pi is powered off, while the non-volatile 
microSD card keeps its contents. 

![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/image--012.png)

3. *MODULE*
    >  This covers the radio, the component which gives the Raspberry Pi the ability to communicate with devices wirelessly. The radio itself acts as two main 
components, in fact: a WiFi radio, for connecting to computer networks; and a Bluetooth radio, for connecting to peripherals like mice and for sending data to
or receiving data from nearby smart devices like sensors or smartphones.

![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/jf.png)

4. *POWER MANAGEMENT INTEGRATED CIRCUIT (PMIC)*
    >  This is the network and USB controller, and is responsible for running the Ethernet port and the four USB ports. It also  handles turning the power 
that comes in from the micro USB port into the power the Pi needs to run.

![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/image--014.png)

5. *RASPBERRY PI's PORTS*
    > These ports let you connect any USB-compatible peripheral, from keyboards and mice to digital cameras and flash drives, to the Pi.
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/image--015.png)

    > You can use this port to connect the Raspberry Pi to a wired computer network using a cable with what is known as an RJ45 connector on its end. If you 
    look closely at the Ethernet port, you’ll see two light-emitting diodes (LEDs) at the bottom; these are status LEDs, and let you know that the connection 
    is working.
    
![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/image--016.png)

6. *AUDIO-VISUAL JACK*
    >  This is also known as the headphone jack, and it can be used for that exact purpose – though you’ll get better sound connecting it to amplified speakers
rather than headphones.

![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/image--017.png) 

7. *CAMERA SERIAL INTERFACE (CSI)*
    > This allows you to use the specially designed Raspberry Pi Camera Module.

![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/image--018.png)

8. *HDMI PORT*
    > You’ll use this to connect the Raspberry Pi to your display device, whether that’s a computer monitor, TV, or projector.

![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/image--021.png)

9. *MICRO USB POWER PORT*
    >  The micro USB port is a common sight on smartphones, tablets, and other portable devices. So you could use a standard mobile charger to power the Pi, but 
for best results you should use the official Raspberry Pi USB Power Supply.

![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/image--022.png)

10. *GPIO HEADER*
    >  40 metal pins, split into two rows of 20 pins (Figure 1-14). This is the GPIO (general-purpose input/output) header, a feature of the Raspberry Pi used to 
talk to additional hardware from LEDs and buttons all the way to temperature sensors, joysticks, and pulse-rate monitors.

![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/image--025.png)

11. *MICRO SD CARD CONNECTOR*
    > . This is the Raspberry Pi’s storage: the microSD card inserted in here contains all the files you save, all the software you install, and the operating 
system that makes the Raspberry Pi run.

![](https://gitlab.com/vivekvar_19/attendence-system-with-iot/-/raw/master/Assignment%201/images1/image--026.png)

Now, we are almost ready to get started with raspberry pi.

For that, first set up the software on your desktop and connect SD card, keyboard, mouse, display and power supply.